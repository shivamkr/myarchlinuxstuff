#!/bin/bash

# Arch Linux - list of some good terminal fonts

repos="adobe-source-code-pro-fonts ttf-inconsolata ttf-hack ttf-dejavu ttf-bitstream-vera terminus-font
";
info=$(pacman -Si $repos | grep -E 'Name|Download\sSize|Description');
for i in $repos; do
	infos=$(echo "$info" | grep -A 2 "$i" | cut -d ":" -f 2 | tail -2 );
	down=$(echo "$infos" | tail -1 );
	desc=$(echo "$infos" | head -1 );
	echo -e "\033[1m$i\033[0m ( $down )\n  - $desc\n";
done
