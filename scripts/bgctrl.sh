#!/bin/bash

# Background Switcher using feh - Use 'bgctrl --next' or 'bgctrl --prev'

CURR_BG=$(cat ~/.fehbg | cut -d "'" -f 2 | tail -1)
FILE=${CURR_BG##*/}
DIR=${CURR_BG%/*}

function prev {
	PREV=$(ls $DIR | grep -B 1 $FILE | head -1)
	[[ $FILE == $PREV ]] && PREV=$(ls $DIR | tail -1)
	feh --bg-fill "$DIR/$PREV" 
}

function next {
	NEXT=$(ls $DIR | grep -A 1 $FILE | tail -1)
	[[ $FILE == $NEXT ]] && NEXT=$(ls $DIR | head -1)
	feh --bg-fill "$DIR/$NEXT" 
}

case "$1" in
"--prev")
    prev
    ;;
"--next")
    next
    ;;
*)
     echo hello
    ;;
esac