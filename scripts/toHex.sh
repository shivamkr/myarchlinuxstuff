#!/bin/sh

# Converts Decimal from STRING, File or STDIN to their Hexadecimal Representation.

printf "\n";

if [ $# -eq 1 -a -f "$1" ]; then
	set -- $(grep -o -e '-\?[0-9]\{1,\}' $1);
elif [ $# -eq 1 -a -n "$1" ]; then
	set -- $(echo "$1" | grep -o -e '-\?[0-9]\{1,\}');
else
	set -- $(grep -o -e '-\?[0-9]\{1,\}' "-");
fi
for i in $*; do
	printf "\t %d (DEC) \t-->\t %x (HEX)\n" $i $i
done

printf "\n";
